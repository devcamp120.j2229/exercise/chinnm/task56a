package com.task56a.restapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task56a.restapi.model.Employee;

@RestController
public class EmployeeController {
    @CrossOrigin
    @GetMapping("/employee")
    public ArrayList<Employee> getEmployee() {
        Employee employee1 = new Employee(1, "chi", "nguyen", 10000000);
        Employee employee2 = new Employee(2, "mai", "tran", 15000000);
        Employee employee3 = new Employee(3, "nam", "do", 20000000);
        ArrayList<Employee> arrList = new ArrayList<>();
        arrList.add(employee1);
        arrList.add(employee2);
        arrList.add(employee3);
        return arrList;

    }

}
