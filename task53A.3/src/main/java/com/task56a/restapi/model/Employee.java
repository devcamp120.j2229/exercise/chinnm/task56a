package com.task56a.restapi.model;

public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private int salary;

    public Employee(int id, String firstName, String lastName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getSalary() {
        return salary;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getAnnnualSalary() {
        return salary * 12;
    }

    public int raiseSalary(int percent) {
        double newSalary = salary + salary * percent * 0.01;
        int newSalaryInt = (int) newSalary;
        return newSalaryInt;
    }

    @Override
    public String toString() {
        return String.format("Employee [ id= %s", id + "name = " + firstName + lastName + "salary=%s ", salary + "]");
    }

}
