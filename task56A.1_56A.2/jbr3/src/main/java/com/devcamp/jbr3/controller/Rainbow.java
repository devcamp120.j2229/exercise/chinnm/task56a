package com.devcamp.jbr3.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Rainbow {
    @CrossOrigin
    @GetMapping("/rainbow")
    public ArrayList<String> getRainbowArr(@RequestParam(value = "username", defaultValue = "Pizza Lover") String name,
            @RequestParam(value = "username1", defaultValue = "Pizza Lover") String name1) {
        ArrayList<String> arrList = new ArrayList();
        arrList.add("red");
        arrList.add("orange");
        arrList.add("yellow");
        arrList.add("green");
        arrList.add("blue");
        arrList.add("indigo");
        arrList.add("violet");
        arrList.add(name);
        arrList.add(name1);
        return arrList;
    }

    @CrossOrigin
    @GetMapping("/split")
    public ArrayList<String> getSplit(
            @RequestParam(value = "username", defaultValue = "Using Arrays.asList() method - Pass the required array to this method and get a List object and pass it as a parameter to the constructor of the ArrayList class.") String name) {
        ArrayList<String> arrList = new ArrayList();
        Collections.addAll(arrList, name.split(" ", 8));

        return arrList;
    }
}
