package com.a7.restapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a7.restapi.model.Rectangle;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("/length")
    public int getLength(@RequestParam(value = "length") String StringInput) {
        return StringInput.length();

    }

    @CrossOrigin
    @GetMapping("/checknumber")
    public String checkNumber(@RequestParam(value = "number") int NumberInput) {
        if (NumberInput % 2 == 0) {
            return NumberInput + " is even";
        } else {
            return NumberInput + " is odd";
        }

    }

    @CrossOrigin
    @GetMapping("/rectangle-area")
    public double getArea(@RequestParam(value = "length") float lengthNumber,
            @RequestParam(value = "width") float widthNUmber) {
        Rectangle rectangle = new Rectangle(lengthNumber, widthNUmber);
        return rectangle.getArea();

    }

    @CrossOrigin
    @GetMapping("/rectangle-perimeter")
    public double getPerimeter(@RequestParam(value = "length") float lengthNumber,
            @RequestParam(value = "width") float widthNUmber) {
        Rectangle rectangle = new Rectangle(lengthNumber, widthNUmber);
        return rectangle.getPerimerter();

    }

}
