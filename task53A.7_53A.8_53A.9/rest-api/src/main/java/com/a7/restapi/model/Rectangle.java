package com.a7.restapi.model;

public class Rectangle {
    private float length = 1.0f;
    private float width = 1.0f;

    public Rectangle(float length, float width) {
        this.length = length;
        this.width = width;
    }

    public Rectangle() {
    }

    public float getLength() {
        return length;
    }

    public float getWidth() {
        return width;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public double getPerimerter() {
        return (length + width) * 2;
    }

    public double getArea() {
        return length * width;
    }

    @Override
    public String toString() {
        return String.format("Rectangle [length = %s, width = %s,", length, width);
    }

}
