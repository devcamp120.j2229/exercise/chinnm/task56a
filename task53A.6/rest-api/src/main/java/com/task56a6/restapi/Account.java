package com.task56a6.restapi;

import org.springframework.util.backoff.BackOff;

public class Account {
    private String id;
    private String name;
    private int blance = 0;

    public Account(String id, String name, int blance) {
        this.id = id;
        this.name = name;
        this.blance = blance;
    }

    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBlance() {
        return blance;
    }

    public int credit(int amount) {
        this.blance += amount;
        return this.blance;
    }

    public int debit(int amount) {
        if (amount <= blance) {
            this.blance -= amount;
        } else {
            System.out.println("Amount exceed blance");

        }
        return this.blance;
    }

    public int transfer(Account another, int amount) {
        if (amount <= blance) {
            another.blance += amount;
            this.blance -= amount;
        } else {
            System.out.println("Amount exceed blance");

        }
        return this.blance;
    }

    @Override
    public String toString() {
        return String.format("Name [ id= %s, name =%s, balance = %s]", id, name, blance);
    }

}
