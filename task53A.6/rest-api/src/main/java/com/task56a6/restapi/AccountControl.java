package com.task56a6.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountControl {
    @CrossOrigin
    @GetMapping("/accounts")
    public ArrayList<Account> getListAcount() {
        Account account1 = new Account("001", "Lan", 100000);
        Account account2 = new Account("002", "Hoang", 200000);
        Account account3 = new Account("003", "Dao", 300000);
        ArrayList<Account> accounts = new ArrayList<>();
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        return accounts;

    }

}
