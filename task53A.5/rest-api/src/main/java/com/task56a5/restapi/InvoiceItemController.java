package com.task56a5.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InvoiceItemController {
    @CrossOrigin
    @GetMapping("/invoices")
    public ArrayList<InvoiceItem> getListInvoiceItem() {
        InvoiceItem hoaDon1 = new InvoiceItem("HD01", "Ti vi", 2, 78000000.0);
        InvoiceItem hoaDon2 = new InvoiceItem("HD02", "May Giat", 3, 45000000.0);
        InvoiceItem hoaDon3 = new InvoiceItem("HD03", "Quat", 12, 800000.0);

        ArrayList<InvoiceItem> invoiceItem = new ArrayList<>();
        invoiceItem.add(hoaDon1);
        invoiceItem.add(hoaDon2);
        invoiceItem.add(hoaDon3);

        return invoiceItem;

    }

}
