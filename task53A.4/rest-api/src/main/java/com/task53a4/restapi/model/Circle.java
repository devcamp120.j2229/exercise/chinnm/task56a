package com.task53a4.restapi.model;

public class Circle {
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return Math.pow(this.radius, 2) * Math.PI;

    }

    public double getCircumference() {
        return this.radius * 2 * Math.PI;

    }

    @Override
    public String toString() {
        return "Circle[" + this.radius + "]";
    }

}
