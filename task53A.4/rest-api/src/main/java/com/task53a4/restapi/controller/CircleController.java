package com.task53a4.restapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.task53a4.restapi.model.Circle;

@RestController
public class CircleController {
    @CrossOrigin
    @GetMapping("circle-area")
    public double getCircleArea(@RequestParam(value = "number") double number) {
        Circle circle1 = new Circle(number);
        double area = circle1.getArea();
        return area;
    }

}
